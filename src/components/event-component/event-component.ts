import { NavParams } from 'ionic-angular';
import { EventService } from './../../providers/event-service';
import { Component } from '@angular/core';

/*
  Generated class for the EventComponent component.

  See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
  for more info on Angular 2 Components.
*/
@Component({
  selector: 'event-component',
  templateUrl: 'event-component.html',
  providers: [EventService]
})
export class EventComponentComponent {

  id;
  event;

  constructor(public eventService: EventService, public navParams: NavParams) {
    
  }

  ionViewDidLoad() {
    this.id = this.navParams.get('eventId');
    this.eventService.getEvent(this.id).subscribe(
      data => { this.event = data; console.log(data); },
      error => { console.log(error); }
    )
  }
}
