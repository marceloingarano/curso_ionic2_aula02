import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

/*
  Generated class for the EventService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class EventService {

  apiUrl = 'http://ionic.apidev.xyz';

  constructor(public http: Http) {
    console.log('Hello EventService Provider');
  }

  get(): Observable<any> {
    return this.http.get(this.apiUrl + '/events')
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json.error));
  }

  getEvent(eventId): Observable<any> {
    return this.http.get(this.apiUrl + '/events?id=' + eventId)
      .map(res => res.json())
      .catch((error: any) => Observable.throw(error.json.error));
  }
}
