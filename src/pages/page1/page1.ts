import { EventComponentComponent } from './../../components/event-component/event-component';
import { AlertController } from 'ionic-angular';
import { EventService } from './../../providers/event-service';
import { Component } from '@angular/core';
import { Geolocation } from 'ionic-native';

import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-page1',
  templateUrl: 'page1.html',
  providers: [EventService]
})
export class Page1 {

  events;

  constructor(public navCtrl: NavController, public eventService: EventService, private alertCtrl: AlertController) {
    
  }

  ionViewDidLoad() {
    this.eventService.get().subscribe(
      data => { this.events = data; },
      error => { console.log(error); }
    );
  }

  goToEvent(eventId) {
    this.navCtrl.push(EventComponentComponent, {
      eventId: eventId
    });
  }

  showCoordinates() { 
    
    Geolocation.getCurrentPosition().then(pos => {
      let alertMsg = 'lat: ' + pos.coords.latitude + ', lon: ' + pos.coords.longitude;
      
      let alert = this.alertCtrl.create({
        title: 'Low battery',
        subTitle: alertMsg,
        buttons: ['Dismiss']
      });

      alert.present();
      console.log('lat: ' + pos.coords.latitude + ', lon: ' + pos.coords.longitude);
    });
  }

}
